package ru.itis.models;

import java.util.StringJoiner;

/**
 * 08.07.2021
 * 01. DB
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Car {
    private Integer id;
    private String color;
    private String model;

    private Account owner;

    public Car(Integer id, String color, String model) {
        this.id = id;
        this.color = color;
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Account getOwner() {
        return owner;
    }

    public void setOwner(Account owner) {
        this.owner = owner;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Car.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("color='" + color + "'")
                .add("model='" + model + "'")
                .add("owner=" + owner.getFirstName() + " " + owner.getLastName())
                .toString();
    }
}
