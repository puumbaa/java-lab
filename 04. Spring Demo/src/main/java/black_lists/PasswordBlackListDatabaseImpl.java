package black_lists;

import repositories.BlackListRepositoryJdbcTemplateImpl;

public class PasswordBlackListDatabaseImpl implements PasswordBlackList{

    private BlackListRepositoryJdbcTemplateImpl blackListRepositoryJdbcTemplate;

    @Override
    public boolean contains(String password) {
        return blackListRepositoryJdbcTemplate.isExistsByPassword(password);
    }

    public PasswordBlackListDatabaseImpl(BlackListRepositoryJdbcTemplateImpl blackListRepositoryJdbcTemplate) {
        this.blackListRepositoryJdbcTemplate = blackListRepositoryJdbcTemplate;
    }
}
