package repositories;

import models.BlackList;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;

public class BlackListRepositoryJdbcTemplateImpl implements BlackListRepository{

    //language=SQL
    private static final String SQL_EXISTS_BY_PASSWORD = "SELECT * FROM password_black_list WHERE password = ?";

    private JdbcTemplate jdbcTemplate;

    private final RowMapper<BlackList> blackListRowMapper = (row, rowNum) -> {
        int id = row.getInt("id");
        String password = row.getString("password");
        return new BlackList(id,password);
    };

    public BlackListRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public boolean isExistsByPassword(String password) {
        List<BlackList> query = jdbcTemplate.query(SQL_EXISTS_BY_PASSWORD, blackListRowMapper, password);
        return !query.isEmpty();
    }
}
