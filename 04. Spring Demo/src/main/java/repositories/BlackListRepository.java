package repositories;

public interface BlackListRepository {
    boolean isExistsByPassword(String password);
}
