package validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 04.09.2021
 * 04. Spring Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class EmailValidatorRegexImpl implements EmailValidator {

    private String regex;

    public void setRegex(String regex) {
        this.regex = regex;
    }

    @Override
    public boolean isValid(String email) {
        Matcher matcher = Pattern.compile(regex, Pattern.CASE_INSENSITIVE).matcher(email);
        return matcher.find();
    }
}
