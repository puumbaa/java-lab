package models;

public class BlackList {
    private Integer id;
    private String password;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public BlackList() {
    }

    public BlackList(String password) {
        this.password = password;
    }

    public BlackList(Integer id, String password) {
        this.id = id;
        this.password = password;
    }

    @Override
    public String toString() {
        return "BlackList{" +
                "id=" + id +
                ", password='" + password + '\'' +
                '}';
    }
}
